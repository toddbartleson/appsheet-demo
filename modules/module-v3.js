
const axios = require('axios');

// https://appsheettest1.azurewebsites.net/sample/list
// https://appsheettest1.azurewebsites.net/sample/list?token=b32b3
// http://appsheettest1.azurewebsites.net/sample/detail/27


class UserFilter {
	constructor () {
		log('new UserFilter');

		this.filteredUsers = [];
	}

	filterUsers() {
		return new Promise((resolve, reject) => {
			log('filterUsers');

			return this.fetchUserList()
				.then(() => {
					displayResults(this.filteredUsers);
					resolve();
				});
		});
	}

	fetchUserList(token) {
		return new Promise((resolve, reject) => {
			log('fetchUserList');
	
			let url = 'https://appsheettest1.azurewebsites.net/sample/list';
			if (token) {
				url += `?token=${token}`;
			}
			log(url);
	
			return axios
				.get(url)
				.then(response => {
					const data = response.data;
	
					log(data);
	
					const promises = [];
	
					/*
					If you comment out the following line you're left with a recursive promise chain that fetches
					user lists until the list api doesn't return a token.
					A recursive promise chain is a new pattern to me, and an interesting thing to implement.
					As a general rule, I avoid recursion, it's a subtle technique that can be tricky to reason about
					and bite your ass due to the size data set even if the algorithm is correct.

					This would be the place to offload the collection of user details requests.

					The real question in my mind is do you let the collection of user ids run ahead of 
					the user details, or do you wait for all the details requests (from the current set) 
					to resolve before grabbing the next chunk of list?

					If want to make the problem parallelized for a really big data set, then I think you 
					can't wait.  I think you dispatch sets of user ids for collection by external processes.
					Which is fine, but then how do you know when they've all be collected?

					I thought about keeping a queue of details-being-fetched user ids, that you would
					remove from as the details requests were fulfilled, and when that list is empty
					you know the entire set has been processed.  

					I googled "javascript promise chain garbage collection" and while I didn't find a 
					smoking gun,  a number of the links do indicate that the promise chain memory 
					will grow until the final promise is resolved.
					//*/
					promises.push(...this.generateUserDetailsRequests(data.result));
	
					if (data.token) {
						promises.push(this.fetchUserList(data.token));
					}
				
					return Promise.all(promises).then(resolve);
				})
				.catch(error => reject(error));
		});		
	}

	generateUserDetailsRequests(userIds) {
		const promises = [];
		userIds.forEach(id => promises.push(this.fetchUserDetails(id)));
		
		return promises;	
	}

	fetchUserDetails(userId) {
		return new Promise((resolve, reject) => {
			let url = `https://appsheettest1.azurewebsites.net/sample/detail/${userId}`;	
			log(url);	
	
			return axios
				.get(url)
				.then(response => {				
					this.mergeUserDetails(response.data, resolve);
				})
				.catch(error => {
					//just resolve on an api error
					resolve();
				});
		});		
	}

	mergeUserDetails(user, callback) {
		log(user);
		if (!validatePhoneNumber(user.number)) {
			callback();
			return;
		}

		if (this.filteredUsers.length < 5) {
			this.filteredUsers.push(user);
			callback();
			return;
		}

		const oldestUser = findOldestUser(this.filteredUsers);

		//if the current user is younger than the oldest, then swap
		if (user.age < oldestUser.age) {
			const index = this.filteredUsers.indexOf(oldestUser);
			this.filteredUsers.splice(index, 1, user);
		}

		callback();
	}
}

function findOldestUser(users) {
	return users
		.reduce((agg, u) => {
			if (u.age > agg.age) {
				agg = u;
			}

			return agg;
		}, { age: -1 });
}

// https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s02.html
const phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
function validatePhoneNumber(phoneNumber) {
	if (phoneRegex.test(phoneNumber)) {
		return true;
	} 
	
	return false;
}

//5 youngest users with valid us telephone numbers sorted by name.
//previously filtered by age,
//previously took 5
//sort by name
function displayResults(filteredUsers) {
	console.log('5 youngest users with valid us telephone numbers sorted by name');
	filteredUsers
		.sort((a, b) => a.name.localeCompare(b.name))
		.forEach(u => console.log(`User name: ${u.name}, age: ${u.age}, phone number: ${u.number}`));
}

function log(msg) {
	return;
	console.log(msg);
}

module.exports = { 
	UserFilter, 
	
    //exposed for testing..
	displayResults,
	findOldestUser, 
	validatePhoneNumber
};