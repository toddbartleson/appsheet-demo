
const axios = require('axios');

// https://appsheettest1.azurewebsites.net/sample/list
// https://appsheettest1.azurewebsites.net/sample/list?token=b32b3
// http://appsheettest1.azurewebsites.net/sample/detail/27

//container for aggregated results
const filteredUsers = [];

//keep a reference to the oldest user of the filteredUsers for efficiency
let oldestUser;

function log(msg) {
	return;
	console.log(msg);
}

function fetchUserIds(token) {
	return new Promise((resolve, reject) => {
		let url = 'https://appsheettest1.azurewebsites.net/sample/list';
		if (token) {
			url += `?token=${token}`;
		}
		log(url);

		return axios
			.get(url)
            .then(response => handleUserListResponse(response.data || {}))
            .then(() => resolve(filteredUsers))
			.catch(error => {
				reject(error);
			});
	});		
}

//where response is like:
//{ result: [ userId, ...], token: "" }
function handleUserListResponse(data) {
	let userIds = data.result;

	//make promises for the current set of user details
	//as well as for the next list (if the token is set)
	const promises = [];
	userIds.forEach(id => promises.push(fetchUserDetails(id)));
	
	if (data.token) {
		promises.push(fetchUserIds(data.token));
	}

	return Promise.all(promises);	
}

function fetchUserDetails(userId) {
	return new Promise((resolve, reject) => {
		let url = `https://appsheettest1.azurewebsites.net/sample/detail/${userId}`;	
		log(url);	

		return axios
			.get(url)
			.then(response => {				
				mergeUserDetails(response.data, resolve);
			})
			.catch(error => {
				//just resolve on an api error
				resolve();
			});
	});		
}

function findOldestUser(users) {
	return users
		.reduce((agg, u) => {
			if (u.age > agg.age) {
				agg = u;
			}

			return agg;
		}, { age: -1 });
}

function mergeUserDetails(user, callback) {
	log(user);
	if (!validatePhoneNumber(user.number)) {
		callback();
		return;
	}

	if (filteredUsers.length < 5) {
		filteredUsers.push(user);
		callback();
		return;
	}

	if (!oldestUser) {
		oldestUser = findOldestUser(filteredUsers);
	}

	//if the current user is younger than the oldest, then swap
	if (user.age < oldestUser.age) {
		let index = filteredUsers.indexOf(oldestUser);
		filteredUsers.splice(index, 1, user);
		oldestUser = findOldestUser(filteredUsers);
	}

	callback();
}

// https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s02.html
const phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
function validatePhoneNumber(phoneNumber) {
	if (phoneRegex.test(phoneNumber)) {
		return true;
	} 
	
	return false;
}

//5 youngest users with valid us telephone numbers sorted by name.
//previously filtered by age,
//previously took 5
//sort by name
function collateResults(filteredUsers) {
	console.log('5 youngest users with valid us telephone numbers sorted by name');
	filteredUsers
		.sort((a, b) => a.name.localeCompare(b.name))
		.forEach(u => console.log(`User name: ${u.name}, age: ${u.age}, phone number: ${u.number}`));
}

module.exports = { 
    fetchFilteredUsers: fetchUserIds, collateResults
    //exposed for testing..
    , handleUserListResponse, findOldestUser, mergeUserDetails, validatePhoneNumber
};