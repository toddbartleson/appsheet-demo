#!/usr/bin/nodejs

const axios = require('axios');

console.log('starting');

// v1, by brute force, 
// get all the user ids
// get all the user details
// do the prescribed filtering and sorting operations

// https://appsheettest1.azurewebsites.net/sample/list
// https://appsheettest1.azurewebsites.net/sample/list?token=b32b3
// http://appsheettest1.azurewebsites.net/sample/detail/27

const userIds = [];

function fetchUserIds(token) {
	return new Promise((resolve, reject) => {
		let url = 'https://appsheettest1.azurewebsites.net/sample/list';
		if (token) {
			url += `?token=${token}`;
		}

		return axios
			.get(url)
			.then(response => {
				let results = response.data;

				userIds.push(...results.result);
				
				if (!results.token) {
					resolve(userIds);
					return;
				}

				return fetchUserIds(results.token).then(resolve);
			})
			.catch(error => {
				reject(error);
			});
	});		
}

function fetchUserDetails(userId) {
	return new Promise((resolve, reject) => {
		let url = `https://appsheettest1.azurewebsites.net/sample/detail/${userId}`;

		return axios
			.get(url)
			.then(response => {
				resolve(response.data);
			})
			.catch(error => {
				//go ahead and resolve an invalid user on an api error
				//it will be filtered out due to lack of a phone number
				resolve({ id: userId });
			});
	});		
}

function fetchAllUserDetails(userIds) {
	const promises = [];
	userIds.forEach(id => promises.push(fetchUserDetails(id)));
	return Promise.all(promises);
}

// https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s02.html
const phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
function validatePhoneNumber(phoneNumber) {
	if (phoneRegex.test(phoneNumber)) {
		return true;
	} 
	
	return false;
}

//5 youngest users with valid us telephone numbers sorted by name.
//sort by age,
//take 5
//sort by name
function collateResults(userDetails) {
	let filteredUsers = userDetails
		.filter(u => validatePhoneNumber(u.number))
		.sort((a, b) => a.age > b.age ? 1 : a.age < b.age ? -1 : 0)
		.slice(0, 5)
		.sort((a, b) => a.name.localeCompare(b.name));

	console.log('5 youngest users with valid us telephone numbers sorted by name');
	filteredUsers.forEach(u => console.log(`User name: ${u.name}, age: ${u.age}, phone number: ${u.number}`));
}

fetchUserIds()
	.then(userIds => {
		fetchAllUserDetails(userIds)
			.then(userDetails => {
				collateResults(userDetails);
				console.log('done!');
			});
	});
