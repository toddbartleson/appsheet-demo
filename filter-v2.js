#!/usr/bin/nodejs

console.log('starting');

const { fetchFilteredUsers, collateResults } = require('./modules/module-v2');

// v2, more parallel, more testable, generally cleaner

// distribute the id collection and details collection tasks
// such that each user detail request is treated like a complete task
// i.e. the filtering and sorting will be handled as user details are gathered 
// and the required memory footprint will be minimized

// the support functions have been moved into a module to improve testability
// I also decomposed the support functions further, and made them pure where I could
// for example, in v1, the findOldest function is not pure and operates on the 
// aggregration container directly

fetchFilteredUsers()
	.then(filteredUsers => {
		collateResults(filteredUsers);
		console.log('done!');
	});
